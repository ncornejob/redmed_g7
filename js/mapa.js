//para estos talleres de google maps, hay que agregar un certificado tanto para Maps Javascript como para Places API
//estas credenciales se pone en el link de google maps console.cloud.google.com/apis

function myMap() {
	var mapOptions = {
	center: new google.maps.LatLng(51.5, -0.12),
	zoom: 10,
	mapTypeId: google.maps.MapTypeId.HYBRID
	}
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}

function initMap(){
	var pyrmont = {lat: -2.142, lng: -79.9140};
	var panel = document.getElementById('map');
	map = new google.maps.Map(panel,{
		center:pyrmont,
		zoom:16
	});
	
	var service = new google.maps.places.PlacesService(map);
	
	service.nearbySearch({
		location: pyrmont,
		radius: 500,
		types: ['store']
	},callback);

	infowindow = new google.maps.InfoWindow();
}

function callback(results, status){
	if(status === google.maps.places.PlacesServiceStatus.OK){
		for(var i=0; i< results.length; i++){
			createMarker(results[i]);
		}
	}
}

function createMarker(place){
	var marker = new google.maps.Marker({
		map: map,
		position: place.geometry.location
	});

	google.maps.event.addListener(marker,'click', function(){
		infowindow.setContent(place.name);
		infowindow.open(map,this);
		//fnstreetview(place.geometry.location);
	})
}

function fnstreetview(posicion){
	var panorama = new google.maps.StreetViewPanorama(
	document.getElementById('stview'), {
		position: posicion,
		pov: {
			heading: 34,
			pitch: 10
		}
	});
	map.setStreetView(panorama);
}